(function() {
	window.LANG='fr';
	var SUPPORTED_LANGUAGE=['fr', 'en'];

	urlparams=new URLSearchParams(window.location.search);
	if (urlparams.has('lang')) {
		if (urlparams.get('lang')=='en') {
			window.LANG='en';
		}
	}

	var bounds = [[0,0], [1000,1000]];

	var basemaps = {
		Surface: L.tileLayer('./tiles/surface/{z}/{x}/{y}.webp', {
			noWrap: true,
			tileSize: 256,
			minZoom: 0,
			maxZoom: 6,
			tms: false,
			continuousWorld: true,
			maxNativeZoom: 6,
			attribution: '<a href="https://framagit.org/StephanePerso/brmap/" target="_blank">Code</a>: Toubib[🐺] | Maps: proprietary data of Whitemoon Games'
		}),
		Underground: L.tileLayer('./tiles/underground/{z}/{x}/{y}.webp', {
			noWrap: true,
			tileSize: 256,
			minZoom: 0,
			maxZoom: 6,
			tms: false,
			continuousWorld: true,
			maxNativeZoom: 6,
			attribution: '<a href="https://framagit.org/StephanePerso/brmap/" target="_blank">Code</a>: Toubib[🐺] | Maps: proprietary data of Whitemoon Games'
		})
	};

	var groups = {
		events: [new L.LayerGroup(), new L.LayerGroup()],
		mined: [new L.LayerGroup(), new L.LayerGroup()],
		npc: [new L.LayerGroup(), new L.LayerGroup()]
	};
	var searchlayer=new L.LayerGroup();

	var BrIcon = L.Icon.extend({
		options: {
			// shadowUrl: 'leaf-shadow.png',
			iconSize:     [48, 48],
			//shadowSize:   [50, 64],
			iconAnchor:   [24, 44],
			//shadowAnchor: [4, 62],
			popupAnchor:  [0, -50]
		}
	});
	var BrSmallIcon = L.Icon.extend({
		options: {
			// shadowUrl: 'leaf-shadow.png',
			iconSize:     [32, 32],
			//shadowSize:   [50, 64],
			iconAnchor:   [16, 29],
			//shadowAnchor: [4, 62],
			popupAnchor:  [0, -34]
		}
	});
	var icons = {
		'Event': new BrIcon({iconUrl: 'img/star-pin.svg'}),
		'Mined': new BrIcon({iconUrl: 'img/pickaxe-pin.svg'}),
		'NPC': new BrSmallIcon({iconUrl: 'img/person-pin.svg'})
	}


	function loadsvg() {
		const out = [];
		const request = new XMLHttpRequest();
		request.open('GET', './tiles/br-map.svg', false);  // `false` makes the request synchronous
		request.send(null);

		if (request.status === 200) {
			//                          console.log(request.responseText);
		} else {
			return false;
		}

		const parser = new DOMParser();
		const doc = parser.parseFromString(request.responseText, "image/svg+xml");
		featurelist = doc.getElementsByTagName("rect");

		for (var i=0; i< featurelist.length; i++) {
			feature=featurelist[i];
			obj={};
			id = feature.getAttribute('data-ID');
			if (id) {
				obj.id=id;
			}
			if (feature.parentNode.getAttribute('inkscape:label')=='poi-underground') {
				obj.level=1;
			} else {
				obj.level=0;
			}
			obj.type = feature.getAttribute('data-type');
			obj.item = feature.getAttribute('data-name-'+LANG);
			translationwarn='';
			if (!obj.item) {
				for (var l in SUPPORTED_LANGUAGE) {
					translationwarn='<div class="translationwarn">(incomplete translation)</div>';
					obj.item = feature.getAttribute('data-name-'+SUPPORTED_LANGUAGE[l]);
					if (obj.item) {
						break;
					}
				}
			}
			if (!obj.item) {
				console.log("item without name");
				continue;
			}
			obj.x = (parseInt(feature.getAttribute('x')) + 3)/31;
			obj.y = -(parseInt(feature.getAttribute('y')) + 3)/31;
			var group;
			switch (obj.type) {
				case 'Mined':
					group=groups.mined[obj.level];
					break;
				case 'Event':
					group=groups.events[obj.level];
					break;
				case 'NPC':
					group=groups.npc[obj.level];
					break;
				default:
					group=null;
					console.log('unknow POI type "' + obj.type+'"');
			}
			if (group!=null) {
				var marker=L.marker([obj.y, obj.x], {title:obj.item, level:obj.level, icon: icons[obj.type]}).bindPopup('<div class="name">'+obj.item+'</div>\n<div class="coords">\n['+obj.x.toFixed(2)+','+obj.y.toFixed(2)+']</div>'+translationwarn);
				marker.addTo(group);
				marker.addTo(searchlayer);
			}
			
		}
	}
	loadsvg();

	console.log("loading data");
	window.Data = {
		LayerGroups: groups,
		SearchLayer: searchlayer,
		Basemaps: basemaps
	};

}());
